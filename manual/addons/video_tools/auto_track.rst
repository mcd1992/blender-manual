
************************
 Automatic Tracking
************************

Activation
==========

- Open Blender and go to Preferences then the Add-ons tab.
- Click Video Tools then check Auto-track to enable the script.


Usage
=====

#. Setup a normal VFX workspace (Load clip, set scene frames, prefetch, ect.)
#. In the `Auto-track` tool tab adjust settings as needed for clip resolution and framerate.
#. Click 'Autotrack'. This will start the loop for detecting features and tracking forward.
#. Hold ESCAPE if you need to stop autotracking.
#. Once complete you can filter tracks with the builtin tools in the 'Track' tool tab and solve camera motion.
#. I recommend enabling Stephen Leger's 'Refine tracking solution' addon to auto adjust weights of your tracks.

.. reference::

   :Category: Video Tools
   :Description: VFX motion tracking automation.
   :Location: :menuselection:`Movie Clip Editor > Tracking > Clip > Toolbar`
   :File: space_clip_editor_auto_track.py
   :Author: mcd1992

