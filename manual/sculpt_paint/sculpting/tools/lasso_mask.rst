
**********
Lasso Mask
**********

.. reference::

   :Mode:      Sculpt Mode
   :Tool:      :menuselection:`Toolbar --> Lasso Mask`

Creates a new :doc:`Mask </sculpt_paint/sculpting/editing/mask>`
based on a :ref:`lasso selection <tool-select-lasso>`.
Hold :kbd:`Ctrl` to subtract from the mask instead.

This tool is also accessible as a :ref:`shortcut operator <bpy.ops.paint.mask_box_gesture>` on
:kbd:`Shift-Ctrl-LMB` drag.


Tool Settings
=============

Front Faces Only
   Only creates a mask on the faces that face towards the view.
