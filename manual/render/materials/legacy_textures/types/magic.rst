.. _bpy.types.MagicTexture:

*****
Magic
*****

The Magic Texture node is used to add a psychedelic color texture.
It can be used for "Thin Film Interference" if you set *Mapping*
to *Reflection* and use a relatively high *Turbulence*.
The RGB components are generated independently with a sine formula.

.. figure:: /images/render_materials_legacy-textures_types_magic_panel.png

   Magic Texture panels.


Options
=======

Depth
   The depth of the calculation. A higher number results in a long calculation time, but also in finer details.
Turbulence
   The strength of the pattern.
